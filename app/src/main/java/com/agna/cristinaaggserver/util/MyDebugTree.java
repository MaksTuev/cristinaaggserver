package com.agna.cristinaaggserver.util;

import timber.log.Timber;

public class MyDebugTree extends Timber.DebugTree {

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        if (stackTrace.length < 6) {
            throw new IllegalStateException("Synthetic stacktrace didn't have enough elements: are you using proguard?");
        }
        StackTraceElement element = stackTrace[5];

        super.log(priority, tag, String.format("(%s:%s) %s", element.getFileName(), element.getLineNumber(), message), t);
    }

}