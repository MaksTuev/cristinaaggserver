package com.agna.cristinaaggserver.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SettingUtil {
    public static final String EMPTY_STRING_SETTING = "";
    public static final int EMPTY_INT_SETTING = -1;

    public static String getString(Context context, String key) {
        SharedPreferences settings = getDefaultSharedPreferences(context);
        return settings.getString(key, EMPTY_STRING_SETTING);
    }

    public static void putString(Context context, String key, String value) {
        SharedPreferences settings = getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        saveChanges(context, editor);
    }

    public static void putInt(Context context, String key, int value) {
        SharedPreferences settings = getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        saveChanges(context, editor);
    }

    public static int getInt(Context context, String key) {
        SharedPreferences settings = getDefaultSharedPreferences(context);
        return settings.getInt(key, EMPTY_INT_SETTING);
    }

    public static void putBoolean(Context context, String key, boolean value) {
        SharedPreferences settings = getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        saveChanges(context, editor);
    }

    public static boolean getBoolean(Context context, String key, boolean defaultValue) {
        SharedPreferences settings = getDefaultSharedPreferences(context);
        return settings.getBoolean(key, defaultValue);
    }

    private static void saveChanges(Context context, SharedPreferences.Editor editor) {
        editor.commit();
    }

    private static SharedPreferences getDefaultSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }


}
