package com.agna.cristinaaggserver.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.agna.cristinaaggserver.app.App;
import com.agna.cristinaaggserver.app.ApplicationComponent;

/**
 * базовая Activity
 * предоставляет {@link ActivityComponent} для инъекции зависимостей
 * Инжектит view с помощью ButterKnife
 */
public abstract class BaseActivity extends AppCompatActivity {

    //private ActivityComponent activityComponent;

    abstract public int getLayoutId();

    /**
     * метод, в реализации которого следует удовлетворить все зависимости,
     * указанные с помощью аннотаций {@link javax.inject.Inject}
     */
    protected abstract void satisfyDependencies();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        initInjector();
        satisfyDependencies();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * инициализирует ActivityComponent,
     * если вам нужен расширенный компонент, то необходимо переопределить этот метод
     */
    private void initInjector() {
        /*activityComponent = DaggerActivityComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(new ActivityModule(this))
                .build();*/
    }

    /**
     * предоставляет {@link ActivityComponent} для инъекции зависимостей
     *//*
    protected ActivityComponent getActivityComponent() {
        return activityComponent;
    }*/

    protected ApplicationComponent getApplicationComponent() {
        return ((App) getApplication()).getApplicationComponent();
    }

}
