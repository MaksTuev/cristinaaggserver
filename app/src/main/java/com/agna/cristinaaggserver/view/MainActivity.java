package com.agna.cristinaaggserver.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.agna.cristinaaggserver.R;
import com.agna.cristinaaggserver.domain.servise.connection.SensorConnectionManager;
import com.agna.cristinaaggserver.domain.servise.sensor.SensorManager;
import com.agna.cristinaaggserver.domain.servise.sensor.sensor.Sensor;
import com.agna.cristinaaggserver.domain.servise.sensor.sensor.audio.AudioSensorInfo;
import com.agna.cristinaaggserver.realization.sensor.AudioManager;
import com.agna.cristinaaggserver.realization.sensor.MicrophoneSensor;

import javax.inject.Inject;

/**
 *
 */
public class MainActivity extends BaseActivity {
    public static void start(Context context) {
        Intent i = new Intent(context, MainActivity.class);
        context.startActivity(i);
    }

    @Inject
    SensorConnectionManager sensorConnectionManager;
    @Inject
    SensorManager sensorManager;

    @Override
    public int getLayoutId() {
        return R.layout.main_activity;
    }

    @Override
    protected void satisfyDependencies() {
        getApplicationComponent().inject(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startWork();
    }

    private void startWork() {
        Sensor microphone = new MicrophoneSensor(
                new AudioSensorInfo(String.valueOf(System.currentTimeMillis())),
                AudioManager.getInstance().setGetAudioBytesObservable());
        sensorManager.addSensor(microphone);
        AudioManager.getInstance().startRead();
    }
}
