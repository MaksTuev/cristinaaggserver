package com.agna.cristinaaggserver.view;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.agna.cristinaaggserver.R;
import com.agna.cristinaaggserver.domain.servise.network.CristinaClient;
import com.agna.cristinaaggserver.domain.servise.network.response.AuthorizationResponse;
import com.agna.cristinaaggserver.domain.servise.sensor.sensor.audio.AudioSensorData;
import com.agna.cristinaaggserver.domain.servise.sensor.sensor.audio.AudioSensorInfo;

import java.util.Random;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

/**
 *
 */
public class LoginActivity extends BaseActivity{

    @Inject
    CristinaClient networkClient;

    private EditText ipEditText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.d("Login onCreate");
        initView();
    }

    private void initView() {
        View loginBtn = findViewById(R.id.login_login_btn);
        loginBtn.setOnClickListener(this::onLoginClick);

        View testSendBtn = findViewById(R.id.login_test_package);
        testSendBtn.setOnClickListener(this::onTestSendBtnClick);

        View saveIpBtn = findViewById(R.id.login_save_ip_btn);
        saveIpBtn.setOnClickListener(v->networkClient.setIp(ipEditText.getText().toString()));

        ipEditText = (EditText)findViewById(R.id.login_ip_et);
        ipEditText.setText(networkClient.getIp());
    }

    private void onTestSendBtnClick(View view) {
        Random r  =new Random();
        AudioSensorInfo sensorInfo = new AudioSensorInfo("testststs");
        byte[] testData = new byte[1000];
        r.nextBytes(testData);
        AudioSensorData sensorData = new AudioSensorData(
                0,
                System.currentTimeMillis(),
                2000,
                2,
                false,
                testData);
        networkClient.sendData(sensorInfo, sensorData);
    }

    private void onLoginClick(View view) {
        networkClient.authorize()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::startMain, this::onLoginError);
    }

    private void onLoginError(Throwable throwable) {
        Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_LONG).show();
    }

    private void startMain(AuthorizationResponse response){
        MainActivity.start(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.login_activity;
    }

    @Override
    protected void satisfyDependencies() {
        getApplicationComponent().inject(this);
    }
}
