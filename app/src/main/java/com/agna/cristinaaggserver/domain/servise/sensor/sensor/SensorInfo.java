package com.agna.cristinaaggserver.domain.servise.sensor.sensor;

/**
 * Класс, содержащий информацию о сенсоре
 */
public abstract class SensorInfo {
    private String id;

    public SensorInfo(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
