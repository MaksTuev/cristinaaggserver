package com.agna.cristinaaggserver.domain.servise.sensor.sensor;

import com.agna.cristinaaggserver.domain.servise.sensor.SmartSensorDataBuffer;

import rx.Observable;

/**
 * Бвзовый класс сенсора
 * содержит логику буфферизации
 */
public abstract class Sensor<I extends SensorInfo, D extends SensorData> {
    private I sensorInfo;
    private Observable<D> sensorDataObservable;
    private SmartSensorDataBuffer<D> buffer;

    public Sensor(I sensorInfo) {
        buffer = new SmartSensorDataBuffer<>(getMaxBufferDurationInMs());
        this.sensorInfo = sensorInfo;
    }

    protected abstract long getMaxBufferDurationInMs();

    protected void setSensorDataObservable(Observable<D> sensorDataObservable) {
        this.sensorDataObservable = sensorDataObservable
                .flatMap(this::bufferize);
    }

    private Observable<? extends D> bufferize(D data) {
        buffer.put(data);
        if (buffer.isFull()) {
            return Observable.just(buffer.debark());
        } else {
            return Observable.empty();
        }
    }

    public I getSensorInfo() {
        return sensorInfo;
    }

    public Observable<D> getSensorDataObservable() {
        return sensorDataObservable;
    }
}
