package com.agna.cristinaaggserver.domain.servise;

/**
 *  module, containing information about the mode in which the application should work.
 *
 */
public class AppConfiguration{
    private static final String RELEASE_PRODUCTION_CONFIGURATION_KEY = "release_confg";

    public AppConfiguration() {
    }

    public boolean useProductionApi(){
        return true;
    }
}
