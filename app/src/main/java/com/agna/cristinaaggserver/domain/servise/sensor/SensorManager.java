package com.agna.cristinaaggserver.domain.servise.sensor;

import com.agna.cristinaaggserver.domain.servise.sensor.sensor.Sensor;

import rx.Subscription;
import rx.functions.Action1;

/**
 *
 */
public interface SensorManager {
    void addSensor(Sensor<?,?> sensor);

    Subscription subscribeToNewSensor(Action1<Sensor<?,?>> onNewSensor);
}
