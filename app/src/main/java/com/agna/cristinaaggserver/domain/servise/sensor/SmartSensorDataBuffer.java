package com.agna.cristinaaggserver.domain.servise.sensor;

import com.agna.cristinaaggserver.domain.servise.sensor.sensor.SensorData;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Буффер для пакетов данных сенсора
 * Содержит логику формирования выходных пакетов с правильными индексами
 * В буффере могут содержаться только пакеты идущие по порядку, если обнаруживается что пакет был потерян, то
 * все пакеты до потерянного будут удалены
 * Содержит логику слияния предыдущих данных буфера с текущими (нужно в случае неудачной отправки пакета на сервер)
 *
 * !!! Индексы входных и выходных пакетов могут не совпадать
 * !!! в текщей рефлизации при отправке на сервер и восстановлении буфера после ошибки буффер накапливается до максимума,
 * затем очищается (первый пекет будет lastBufferData, в котором будут слиты все предыдущие, и он удалится) для фикса
 * нужно чтобы lastBufferData был List<D>
 */
public class SmartSensorDataBuffer<D extends SensorData> {
    private List<D> sensorDataList = new ArrayList<>();
    private long maxDurationInMs;
    private int outSensorDataIndexProvider = 0;
    private D lastBufferData;

    public SmartSensorDataBuffer(long maxDurationInMs) {
        this.maxDurationInMs = maxDurationInMs;
    }

    /**
     * добавляет пакет в буффер
     * если обнаружится что пердыдущий пакет был потерян, то
     * все пакеты до потерянного будут удалены
     * @param data добавляемый пакет
     */
    public void put(D data) {
        synchronized (this) {
            if (sensorDataList.size() == 0) {
                if (lastBufferData != null && isSensorDataMissed(lastBufferData, data)) {
                    incrementOutDataIndex();
                    //забываем предыдущие данные буфера, т.к между ними и текущими данными образовался разрыв
                    lastBufferData = null;
                    Timber.e("Package lost");
                }
                sensorDataList.add(data);
            } else {
                D lastSensorData = sensorDataList.get(sensorDataList.size() - 1);
                if (isSensorDataMissed(lastSensorData, data)) {
                    sensorDataList.clear();
                    incrementOutDataIndex();
                    Timber.e("Package lost");
                }
                sensorDataList.add(data);
            }
        }
    }

    /**
     * @return пакет, являющийся суммой всех пакетов добавленных в буффер
     * буффер при этом очищается
     */
    public D debark() {
        synchronized (this) {
            D result = null;
            for (D sensorData : sensorDataList) {
                if (result == null) {
                    result = sensorData;
                } else {
                    result = (D)result.merge(sensorData);
                }
            }
            sensorDataList.clear();
            //сохраняем последний выгруженный пакет
            lastBufferData = (D) result.clone();
            result.setIndex(getOutSensorDataIndex());
            incrementOutDataIndex();
            return result;
        }
    }

    /**
     * показывает, заполнен ли буффер
     */
    public boolean isFull() {
        int sumDuration = 0;
        for (D sensorData : sensorDataList) {
            sumDuration += sensorData.getDurationInMs();
        }
        return sumDuration >= maxDurationInMs;
    }

    public boolean isEmpty(){
        return sensorDataList.size() == 0;
    }

    /**
     * добавляет предыдущие данные буфера к текущим (нужно в случае неудачной отправки пакета на сервер)
     */
    public void restoreLastBufferData(){
        synchronized (this) {
            if (lastBufferData != null) {
                sensorDataList.add(0, lastBufferData);
                //следующий отправленны пакет долже быть с таким же индексом как и неотправленный
                decrementOutDataIndex();
                Timber.e("restore last package, time: "+ System.currentTimeMillis());
            }
        }
    }

    /**
     * если буффер переполнен, удаляет первые пакеты пока длитеньность буффера не будет меньше максимальной
     */
    public void tryTrimToMaxDuration(){
        synchronized (this) {
            while (isFull()) {
                removeFirstSensorData();
            }
        }
    }

    private void removeFirstSensorData() {
        if (sensorDataList.size() != 0) {
            sensorDataList.remove(0);
            incrementOutDataIndex();
            //забываем предыдущие данные буфера, т.к между ними и текущими данными образовался разрыв
            lastBufferData = null;
            Timber.e("Package lost");
        }
    }

    /**
     * показывает был ли потерян пакет, между firstSensorData и secondSensorData
     */
    private boolean isSensorDataMissed(D firstSensorData, D secondSensorData) {
        return secondSensorData.getIndex() - firstSensorData.getIndex() != 1;
    }

    private void incrementOutDataIndex(){
        outSensorDataIndexProvider++;
    }

    private void decrementOutDataIndex(){
        outSensorDataIndexProvider--;
    }

    private int getOutSensorDataIndex() {
        return outSensorDataIndexProvider;
    }


}
