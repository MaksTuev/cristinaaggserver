package com.agna.cristinaaggserver.domain.servise.network;

/**
 * interface of module, storing user sessions
 */
public interface SessionStorage {

    boolean isSessionEmpty();

    void saveSession(String session);

    String getSession();

    void clearSessions();
}
