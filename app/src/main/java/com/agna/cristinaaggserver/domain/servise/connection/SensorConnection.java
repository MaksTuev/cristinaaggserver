package com.agna.cristinaaggserver.domain.servise.connection;

import com.agna.cristinaaggserver.domain.servise.network.CristinaClient;
import com.agna.cristinaaggserver.domain.servise.network.response.BaseResponse;
import com.agna.cristinaaggserver.domain.servise.sensor.SmartSensorDataBuffer;
import com.agna.cristinaaggserver.domain.servise.sensor.sensor.Sensor;
import com.agna.cristinaaggserver.domain.servise.sensor.sensor.SensorData;
import com.agna.cristinaaggserver.domain.servise.sensor.sensor.SensorInfo;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import rx.Observable;
import rx.Subscription;

/**
 * Класс инкапсулирующий соединение скнсора с сервером
 * гарантирует отправление пакетов в правильном порядке
 */
public class SensorConnection<I extends SensorInfo, D extends SensorData> {
    private static final long MAX_BUFFER_DURATION_IN_MS = 20 * 1000; //ms

    private final CristinaClient networkClient;
    private final Sensor<I, D> sensor;
    private final SmartSensorDataBuffer<D> dataBuffer;
    private AtomicBoolean sendingToServer = new AtomicBoolean(false);

    private Subscription getSensorDataSubscription;
    private Subscription sendSensorDataSubscription;

    public SensorConnection(Sensor<I, D> sensor, CristinaClient networkClient) {
        dataBuffer = new SmartSensorDataBuffer<>(MAX_BUFFER_DURATION_IN_MS);
        this.sensor = sensor;
        this.networkClient = networkClient;
        connect();
    }

    private void connect() {
        getSensorDataSubscription = sensor.getSensorDataObservable()
                .subscribe(
                        this::onGetSensorData,
                        this::onGetSensorDataError);
    }

    /**
     * Обработчик ошибок получения данных с сенсора
     */
    private void onGetSensorDataError(Throwable throwable) {
        //todo
    }

    /**
     * Обработчик получения данных с сенсора
     */
    private void onGetSensorData(D sensorData) {
        synchronized (this) {
            dataBuffer.put(sensorData);
            dataBuffer.tryTrimToMaxDuration();
            //if (sendSensorDataSubscription == null || sendSensorDataSubscription.isUnsubscribed()) {
            if(!sendingToServer.get()){
                sendSensorData(dataBuffer.debark());
            }
        }
    }

    /**
     * отправляет плок данны на сервер c задержкой
     */
    private void sendSensorData(D data, int delayInMs) {
        sendingToServer.set(true);
        Observable.timer(delayInMs, TimeUnit.MILLISECONDS)
                .flatMap(t -> networkClient.sendData(sensor.getSensorInfo(), data))
                .subscribe(
                        this::onSendDataSuccess,
                        this::onSendDataError);
    }

    /**
     * отправляет плок данны на сервер
     */
    private void sendSensorData(D data) {
        sendSensorData(data, 0);
    }

    private void onSendDataError(Throwable throwable) {
        synchronized (this) {
            sendingToServer.set(false);
            dataBuffer.restoreLastBufferData();
            dataBuffer.tryTrimToMaxDuration();
            sendSensorData(dataBuffer.debark(), 500);
        }
    }

    /**
     * обработчик успешной отправки данных на сервер
     */
    private void onSendDataSuccess(BaseResponse baseResponse) {
        synchronized (this){
            sendingToServer.set(false);
            if (!dataBuffer.isEmpty()) {
                sendSensorData(dataBuffer.debark());
            }
        }
    }

}
