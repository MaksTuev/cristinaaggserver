package com.agna.cristinaaggserver.domain.servise.network;

import com.agna.cristinaaggserver.domain.servise.network.response.AuthorizationResponse;
import com.agna.cristinaaggserver.domain.servise.network.response.BaseResponse;
import com.agna.cristinaaggserver.domain.servise.sensor.sensor.SensorData;
import com.agna.cristinaaggserver.domain.servise.sensor.sensor.SensorInfo;

import rx.Observable;

/**
 * клиент сервера
 */
public interface CristinaClient {

    //temp
    void setIp(String ip);

    //temp
    String getIp();

    Observable<AuthorizationResponse> authorize();

    <I extends SensorInfo, D extends SensorData> Observable<BaseResponse> sendData(I sensorInfo, D data);
}
