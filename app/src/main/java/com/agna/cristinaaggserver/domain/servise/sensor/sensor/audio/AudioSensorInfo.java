package com.agna.cristinaaggserver.domain.servise.sensor.sensor.audio;

import com.agna.cristinaaggserver.domain.servise.sensor.sensor.SensorInfo;

/**
 * информация об аудио сенсоре
 */
public class AudioSensorInfo extends SensorInfo {
    public AudioSensorInfo(String id) {
        super(id);
    }
}
