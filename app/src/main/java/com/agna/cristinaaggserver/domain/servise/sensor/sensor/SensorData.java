package com.agna.cristinaaggserver.domain.servise.sensor.sensor;

/**
 * Пакет данных сенсора
 */
public abstract class SensorData {
    private int index;
    private long timestamp;


    public SensorData(int index, long timestamp) {
        this.index = index;
        this.timestamp = timestamp;
    }

    /**
     * @return длительность пакета в миллисекундах
     */
    public abstract int getDurationInMs();

    /**
     * Соединяет 2 пакета вместе
     * @param nextSensorData пакет, который идет после существуущего
     */
    public abstract SensorData merge(SensorData nextSensorData);

    public abstract SensorData clone();

    public int getIndex() {
        return index;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
