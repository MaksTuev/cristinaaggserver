package com.agna.cristinaaggserver.domain.servise.network.response;

/**
 *
 */
public class AuthorizationResponse extends BaseResponse{
    private String sid;

    public AuthorizationResponse(String sid) {
        this.sid = sid;
    }

    public String getSid() {
        return sid;
    }
}
