package com.agna.cristinaaggserver.domain.servise.sensor.sensor.audio;

import com.agna.cristinaaggserver.domain.servise.sensor.sensor.Sensor;

/**
 * аудио сенсор
 */
public class AudioSensor extends Sensor<AudioSensorInfo, AudioSensorData> {

    public static final int MAX_BUFFER_DURATION_IN_MS = 1000;

    public AudioSensor(AudioSensorInfo sensorInfo) {
        super(sensorInfo);
    }

    @Override
    protected long getMaxBufferDurationInMs() {
        return MAX_BUFFER_DURATION_IN_MS;
    }
}
