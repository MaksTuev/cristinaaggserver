package com.agna.cristinaaggserver.domain.servise.connection;

import com.agna.cristinaaggserver.domain.servise.network.CristinaClient;
import com.agna.cristinaaggserver.domain.servise.sensor.SensorManager;
import com.agna.cristinaaggserver.domain.servise.sensor.sensor.Sensor;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Класс управляющий соединениями сенсоров с сервером
 */
public class SensorConnectionManagerImpl implements SensorConnectionManager {
    private CristinaClient networkClient;
    private SensorManager sensorManager;

    private List<SensorConnection> sensorConnections = new ArrayList<>();

    @Inject
    public SensorConnectionManagerImpl(CristinaClient networkClient, SensorManager sensorManager) {
        this.networkClient = networkClient;
        this.sensorManager = sensorManager;
        sensorManager.subscribeToNewSensor(this::onNewSensorListener);
    }

    /**
     * обработчик подключения нового сенсора
     */
    private void onNewSensorListener(Sensor<?,?> newSensor) {
        subscribeToSensorData(newSensor);
        SensorConnection<?,?> sensorConnection = new SensorConnection<>(newSensor, networkClient);
        sensorConnections.add(sensorConnection);
    }

    private void subscribeToSensorData(Sensor sensor) {
        //todo handle sensor disconnected
    }
}
