package com.agna.cristinaaggserver.domain.servise.sensor;

import com.agna.cristinaaggserver.domain.servise.sensor.sensor.Sensor;
import com.agna.cristinaaggserver.domain.util.rx.SimpleOnSubscribe;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;

/**
 * Класс, управляющий сенсорами
 */
public class SensorManagerImpl implements SensorManager {
    private List<Sensor<?,?>> sensors = new ArrayList<>();
    private SimpleOnSubscribe<Sensor<?,?>> newSensorEmitter = new SimpleOnSubscribe<>();
    private Observable<Sensor<?,?>> newSensorObservable = Observable.create(newSensorEmitter);

    @Inject
    public SensorManagerImpl() {
    }

    @Override
    public void addSensor(Sensor<?,?> sensor){
        this.sensors.add(sensor);
        subscribeToSensorDataObservable(sensor);
        newSensorEmitter.emit(sensor);
    }

    /**
     * Обработка разрыва соединения с сенсором
     * @param sensor
     */
    private void subscribeToSensorDataObservable(Sensor<?,?> sensor) {
        //todo
    }

    /**
     * Подписывает на событие получения нового сенсора
     * @return Subscription, если подписывается временный обьект (Activity, Form ...)
     * то нужно явно вызвать {@link Subscription#unsubscribe()} в конце его жизни для предотвращении утечки памяти
     */
    @Override
    public Subscription subscribeToNewSensor(Action1<Sensor<?,?>> onNewSensor){
        return newSensorObservable.subscribe(onNewSensor);
    }

}
