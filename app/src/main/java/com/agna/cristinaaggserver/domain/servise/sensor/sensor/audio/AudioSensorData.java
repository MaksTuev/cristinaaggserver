package com.agna.cristinaaggserver.domain.servise.sensor.sensor.audio;

import com.agna.cristinaaggserver.domain.servise.sensor.sensor.SensorData;

/**
 *
 */
public class AudioSensorData extends SensorData {
    private int frequency;
    private int bytePerSecond;
    private byte[] data;
    private boolean bigEndian;

    public AudioSensorData(int index, long timestamp, int frequency, int bytePerSecond, boolean bigEndian, byte[] data) {
        super(index, timestamp);
        this.frequency = frequency;
        this.bytePerSecond = bytePerSecond;
        this.data = data;
        this.bigEndian = bigEndian;
    }

    @Override
    public int getDurationInMs() {
        return (int) (1000.0f * data.length / (bytePerSecond * frequency));
    }

    @Override
    public SensorData merge(SensorData nextSensorData) {
        AudioSensorData nextAudioSensorData = (AudioSensorData)nextSensorData;
        byte[] resultData = new byte[data.length + nextAudioSensorData.data.length];
        int i = 0;
        for(int j = 0; j<data.length; j++, i++){
            resultData[i] = data[j];
        }
        for(int j = 0; j<nextAudioSensorData.data.length; j++, i++){
            resultData[i] = nextAudioSensorData.data[j];
        }
        return new AudioSensorData(
                nextAudioSensorData.getIndex(),
                this.getTimestamp(),
                this.frequency,
                this.bytePerSecond,
                this.bigEndian,
                resultData);
    }

    @Override
    public SensorData clone() {
        return new AudioSensorData(getIndex(), getTimestamp(), frequency, bytePerSecond, this.bigEndian, data.clone());
    }

    public int getFrequency() {
        return frequency;
    }

    public int getBytePerSecond() {
        return bytePerSecond;
    }

    public byte[] getData() {
        return data;
    }

    public boolean isBigEndian() {
        return bigEndian;
    }
}
