package com.agna.cristinaaggserver.realization.network;

import com.agna.cristinaaggserver.domain.servise.network.response.AuthorizationResponse;
import com.agna.cristinaaggserver.domain.servise.network.response.BaseResponse;

import java.util.HashMap;

import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import rx.Observable;

/**
 * интерфейс клиента для retrofit
 */
public interface CristinaApi {
    @GET("/session/start")
    Observable<AuthorizationResponse> authorize();

    @FormUrlEncoded
    @POST("/data/write")
    Observable<BaseResponse> sendAudioSensorData(
            @FieldMap() HashMap<String, String> params);
            //@Part("data") TypedInput data);
}
