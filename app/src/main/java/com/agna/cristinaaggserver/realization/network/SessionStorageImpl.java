package com.agna.cristinaaggserver.realization.network;

import android.content.Context;

import com.agna.cristinaaggserver.domain.servise.network.SessionStorage;
import com.agna.cristinaaggserver.util.SettingUtil;

import javax.inject.Inject;

/**
 * interface of module, storing user sessions
 */
public class SessionStorageImpl implements SessionStorage{
    private static final String KEY_SESSION = "session";

    private Context appContext;

    @Inject
    public SessionStorageImpl(Context appContext) {
        this.appContext = appContext;
    }


    @Override
    public boolean isSessionEmpty() {
        return getSession().equals(SettingUtil.EMPTY_STRING_SETTING);
    }

    @Override
    public void saveSession(String session) {
        SettingUtil.putString(appContext, KEY_SESSION, session);
    }

    @Override
    public String getSession() {
        return SettingUtil.getString(appContext, KEY_SESSION);
    }

    @Override
    public void clearSessions() {
        SettingUtil.putString(appContext, KEY_SESSION, SettingUtil.EMPTY_STRING_SETTING);
    }
}
