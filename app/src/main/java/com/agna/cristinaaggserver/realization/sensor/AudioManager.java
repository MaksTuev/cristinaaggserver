package com.agna.cristinaaggserver.realization.sensor;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

import com.agna.cristinaaggserver.domain.util.rx.AsyncUtil;
import com.agna.cristinaaggserver.domain.util.rx.SimpleOnSubscribe;

import java.util.Arrays;

import rx.Observable;

/**
 *
 */
public class AudioManager {
    private static AudioManager instance;
    private AudioRecord audioRecord;
    int myBufferSize = 8192;
    SimpleOnSubscribe<byte[]> getAudioBytesOnSubscribe = new SimpleOnSubscribe<>();
    Observable<byte[]> getAudioBytesObservable = Observable.create(getAudioBytesOnSubscribe);


    private AudioManager() {
        createAudioRecorder();
    }

    public static AudioManager getInstance() {
        if(instance==null){
            instance = new AudioManager();
        }
        return instance;
    }

    void createAudioRecorder() {
        int sampleRate = 8000;

        int channelConfig = AudioFormat.CHANNEL_IN_MONO;
        int audioFormat = AudioFormat.ENCODING_PCM_16BIT;

        int minInternalBufferSize = AudioRecord.getMinBufferSize(sampleRate,
                channelConfig, audioFormat);
        int internalBufferSize = minInternalBufferSize * 4;

        audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                sampleRate, channelConfig, audioFormat, internalBufferSize);
    }

    public Observable<byte[]> setGetAudioBytesObservable(){
        return getAudioBytesObservable;
    }

    public void startRead() {
        audioRecord.startRecording();
        AsyncUtil.runIO(()-> {
            if (audioRecord == null)
                return null;

            byte[] myBuffer = new byte[myBufferSize];
            int readCount = 0;
            boolean read = true;
            while (read) {
                readCount = audioRecord.read(myBuffer, 0, myBufferSize);
                getAudioBytesOnSubscribe.emit(Arrays.copyOfRange(myBuffer, 0, readCount));//myBuffer.clone());
                //Thread.sleep(50);
            }
            return null;
        });
    }
}
