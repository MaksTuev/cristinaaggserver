package com.agna.cristinaaggserver.realization.network;

import com.agna.cristinaaggserver.BuildConfig;
import com.agna.cristinaaggserver.domain.servise.AppConfiguration;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * base class for working with server
 */
public abstract class BaseNetworkClient {
    protected static final String ROOT_PRODUCTION = "http://192.168.0.100:9000/api/";
    protected static final String ROOT_DEVELOPMENT = "http://hattip-dev.herokuapp.com/api/v1";


    protected String ip = "192.168.0.100:9000";
    private AppConfiguration appConfiguration;

    public BaseNetworkClient(AppConfiguration appConfiguration) {
        this.appConfiguration = appConfiguration;
        setupRestClient();
    }

    abstract protected void setupRestClient();


    protected String getRootUrl() {
        return appConfiguration.useProductionApi()
                ? "http://"+ip+"/api/" //ROOT_PRODUCTION
                : ROOT_DEVELOPMENT;
    }

    protected Gson getParser() {
        Gson gson = new GsonBuilder()
                .create();
        return gson;
    }

    protected AppConfiguration getAppConfiguration() {
        return appConfiguration;
    }

    protected <T> T createApi(String endpoint, Class<T> apiInterface) {
        RestAdapter.Builder restBuilder = new RestAdapter.Builder()
                .setEndpoint(endpoint)
                .setClient(new OkClient(new OkHttpClient()))
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .setConverter(new GsonConverter(getParser()));
        RestAdapter restAdapter = restBuilder.build();
        return restAdapter.create(apiInterface);
    }
}
