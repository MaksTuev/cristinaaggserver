package com.agna.cristinaaggserver.realization.sensor;

import com.agna.cristinaaggserver.domain.servise.sensor.sensor.audio.AudioSensor;
import com.agna.cristinaaggserver.domain.servise.sensor.sensor.audio.AudioSensorData;
import com.agna.cristinaaggserver.domain.servise.sensor.sensor.audio.AudioSensorInfo;

import rx.Observable;
import timber.log.Timber;

/**
 *
 */
public class MicrophoneSensor extends AudioSensor {
    private static final int OUTPUT_FREQUENCY = 2000;
    private static final int INPUT_FREQUENCY = 8000;
    private static final int BYTE_PER_SECOND = 2;
    private int sensorDataIndexProvider = 0;

    public MicrophoneSensor(AudioSensorInfo sensorInfo, Observable<byte[]> audioDataObservable) {
        super(sensorInfo);
        setSensorDataObservable(createSensorDataObservable(audioDataObservable));
    }

    private Observable<AudioSensorData> createSensorDataObservable(Observable<byte[]> audioDataObservable) {
        return audioDataObservable
                .map(this::reduceFrequency)
                .map(this::createSensorData);
    }

    private AudioSensorData createSensorData(byte[] bytes) {
        return new AudioSensorData(sensorDataIndexProvider++, 0, OUTPUT_FREQUENCY, BYTE_PER_SECOND, false, bytes);

    }


    private byte[] reduceFrequency(byte[] bytes) {
        float frequencyRatio = 1.0f * OUTPUT_FREQUENCY/INPUT_FREQUENCY;
        byte[] result = new byte[(int)(frequencyRatio*bytes.length)];
        int delta = (int)(1/frequencyRatio)*BYTE_PER_SECOND;
        Timber.d("Input length: "+ bytes.length+ " Output length: "+ result.length);
        for(int i = 0, j = 0; i<bytes.length; i+=delta){
            result[j] = bytes[i];
            j++;
            result[j] = bytes[i+1];
            j++;
            //Timber.d("i= "+i+ " j= "+j);
        }
        return result;
    }
}
