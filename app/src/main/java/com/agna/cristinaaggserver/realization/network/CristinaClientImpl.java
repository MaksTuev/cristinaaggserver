package com.agna.cristinaaggserver.realization.network;

import com.agna.cristinaaggserver.domain.servise.AppConfiguration;
import com.agna.cristinaaggserver.domain.servise.network.CristinaClient;
import com.agna.cristinaaggserver.domain.servise.network.SessionStorage;
import com.agna.cristinaaggserver.domain.servise.network.response.AuthorizationResponse;
import com.agna.cristinaaggserver.domain.servise.network.response.BaseResponse;
import com.agna.cristinaaggserver.domain.servise.sensor.sensor.SensorData;
import com.agna.cristinaaggserver.domain.servise.sensor.sensor.SensorInfo;
import com.agna.cristinaaggserver.domain.servise.sensor.sensor.audio.AudioSensorData;
import com.agna.cristinaaggserver.domain.servise.sensor.sensor.audio.AudioSensorInfo;

import java.nio.charset.Charset;
import java.util.HashMap;

import javax.inject.Inject;

import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;
import rx.Observable;
import timber.log.Timber;

/**
 *
 */
public class CristinaClientImpl extends BaseNetworkClient implements CristinaClient {

    private final SessionStorage sessionStorage;
    private CristinaApi api;

    @Inject
    public CristinaClientImpl(AppConfiguration appConfiguration, SessionStorage sessionStorage) {
        super(appConfiguration);
        this.sessionStorage = sessionStorage;
    }

    @Override
    protected void setupRestClient() {
        api = createApi(getRootUrl(), CristinaApi.class);
    }

    protected String getSid() {
        return sessionStorage.getSession();
    }

    @Override
    public void setIp(String ip) {
        this.ip = ip;
        setupRestClient();
    }

    @Override
    public String getIp() {
        return ip;
    }

    @Override
    public Observable<AuthorizationResponse> authorize() {
        return api.authorize()
                .doOnNext(this::saveSession);
    }

    private void saveSession(AuthorizationResponse authorizationResponse) {
        sessionStorage.saveSession(authorizationResponse.getSid());
    }

    @Override
    public <I extends SensorInfo, D extends SensorData> Observable<BaseResponse> sendData(I sensorInfo, D sensorData) {
        Timber.d("send package, index: " + sensorData.getIndex() + " time: " + System.currentTimeMillis());
        if (sensorInfo instanceof AudioSensorInfo) {
            return sendAudioSensorData((AudioSensorInfo) sensorInfo, (AudioSensorData) sensorData);
        }
        throw new RuntimeException("Sensor not supported, SensorInfo: " + sensorInfo);
    }

    private Observable<BaseResponse> sendAudioSensorData(AudioSensorInfo sensorInfo, AudioSensorData sensorData) {
        TypedInput data = new TypedByteArray("octet/stream", sensorData.getData());
        HashMap<String, String> params = new HashMap<>();
        params.put("sid", getSid());
        params.put("sensorId", sensorInfo.getId());
        params.put("frequency", String.valueOf(sensorData.getFrequency()));
        params.put("bps", String.valueOf(sensorData.getBytePerSecond()));
        params.put("index", String.valueOf(sensorData.getIndex()));
        params.put("isBigEndian", String.valueOf(sensorData.isBigEndian()));
        params.put("dataSize", String.valueOf(sensorData.getData().length));
        params.put("data", new String(sensorData.getData(), Charset.forName("ISO-8859-1")));
        return api.sendAudioSensorData(params);

    }
}
