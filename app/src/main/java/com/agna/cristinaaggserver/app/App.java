package com.agna.cristinaaggserver.app;

import android.app.Application;

import com.agna.cristinaaggserver.BuildConfig;
import com.agna.cristinaaggserver.R;
import com.agna.cristinaaggserver.util.MyDebugTree;

import timber.log.Timber;

/**
 *
 */
public class App extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initLog();
        initInjector();
    }

    private void initInjector() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    private void initLog() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new MyDebugTree());
        }
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }
}
