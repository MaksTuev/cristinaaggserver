/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.agna.cristinaaggserver.app;

import android.content.Context;

import com.agna.cristinaaggserver.domain.servise.AppConfiguration;
import com.agna.cristinaaggserver.domain.servise.connection.SensorConnectionManager;
import com.agna.cristinaaggserver.domain.servise.network.CristinaClient;
import com.agna.cristinaaggserver.domain.servise.network.SessionStorage;
import com.agna.cristinaaggserver.domain.servise.sensor.SensorManager;
import com.agna.cristinaaggserver.view.LoginActivity;
import com.agna.cristinaaggserver.view.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Основной компонент, удовлетворяет большинство зависимостей
 */
@Singleton // Constraints this component to one-per-application or unscoped bindings.
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(LoginActivity loginActivity);

    void inject(MainActivity mainActivity);

    //Exposed to sub-graphs.
    Context context();

    CristinaClient cristinaClient();

    SessionStorage sessionStorage();

    SensorManager sensorManager();

    SensorConnectionManager sensorConnectionManager();

    AppConfiguration appConfiguration();
    //todo add other objects

}
