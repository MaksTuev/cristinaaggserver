/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.agna.cristinaaggserver.app;

import android.content.Context;

import com.agna.cristinaaggserver.domain.servise.AppConfiguration;
import com.agna.cristinaaggserver.domain.servise.connection.SensorConnectionManager;
import com.agna.cristinaaggserver.domain.servise.connection.SensorConnectionManagerImpl;
import com.agna.cristinaaggserver.domain.servise.network.CristinaClient;
import com.agna.cristinaaggserver.domain.servise.network.SessionStorage;
import com.agna.cristinaaggserver.domain.servise.sensor.SensorManager;
import com.agna.cristinaaggserver.domain.servise.sensor.SensorManagerImpl;
import com.agna.cristinaaggserver.realization.network.CristinaClientImpl;
import com.agna.cristinaaggserver.realization.network.SessionStorageImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Даггер модуль, предоставляющий основные обьекты, которые будут существовать в течении жизненного цикла приложения.
 */
@Module
public class ApplicationModule {
    private final Context applicationContext;

    public ApplicationModule(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return this.applicationContext;
    }

    @Provides
    @Singleton
    CristinaClient provideCristinaClient(CristinaClientImpl cristinaClient) {
        return cristinaClient;
    }

    @Provides
    @Singleton
    SessionStorage provideSessionStorage(SessionStorageImpl sessionStorage) {
        return sessionStorage;
    }

    @Provides
    @Singleton
    SensorConnectionManager provideSensorConnectionManager(SensorConnectionManagerImpl sensorConnectionManager) {
        return sensorConnectionManager;
    }

    @Provides
    @Singleton
    SensorManager provideSensorManager(SensorManagerImpl sensorManager) {
        return sensorManager;
    }

    @Provides
    @Singleton
    AppConfiguration provideAppConfiguration() {
        return new AppConfiguration();
    }
}
